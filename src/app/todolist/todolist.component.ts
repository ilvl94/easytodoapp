import { Component, OnInit } from '@angular/core';



@Component({
  selector: 'app-todolist',
  templateUrl: './todolist.component.html',
  styleUrls: ['./todolist.component.css']
})
export class TodolistComponent implements OnInit {





    todos = [


  { title: "Buy an insurance", done: true },
  { title: "Print tickets", done: false },
  { title: "Call to Veronica", done: true },
  { title: "Create angular easy todo app", done: false }



]


  isHidden = true;





  newtodoname = '';



  droptodos() {
   
    this.todos = this.todos.filter(i => i.done === false);
  }


  showError() {
    console.log("error")
  }

  addNew() {

    if (this.newtodoname.length < 4) {

      this.isHidden = false;
    } else {



      this.todos.push({ title: this.newtodoname, done: false });
      console.log(this.todos);
      this.isHidden = true;
      this.newtodoname = ''
    }
  }



  constructor() { }

  ngOnInit(): void {

  }

}
